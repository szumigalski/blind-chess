import {configureStore} from '@reduxjs/toolkit';
import settingsReducer from './slices/settingsSlice';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {persistReducer} from 'redux-persist';
import {combineReducers} from 'redux';
import thunk from 'redux-thunk';
import {SettingsInterface} from './slices/settingsSlice';
import gameReducer, {GameReducer} from './slices/gamesSlice';
import lichessSlice from './slices/lichessSlice';
import userSlice from './slices/userSlice';

export const reducers = combineReducers({
  settings: settingsReducer,
  game: gameReducer,
  lichess: lichessSlice,
  user: userSlice,
});
export interface LichessReducer {
  error: string | null;
}

export interface UserReducer {
  token: string | null;
  error: string | undefined;
  username: null | UserInfo;
  loading: string | null;
}

export interface ReducerInterface {
  settings: SettingsInterface;
  game: GameReducer;
  lichess: LichessReducer;
  user: UserReducer;
}

interface lichessRank {
  games: number;
  rating: number;
  rd: number;
  prog: number;
  prov?: boolean;
}

export interface UserInfo {
  id: string;
  username: string;
  perfs: {
    blitz: lichessRank;
    storm: {
      runs: number;
      score: number;
    };
    puzzle: lichessRank;
    bullet: lichessRank;
    correspondence: lichessRank;
    classical: lichessRank;
    rapid: lichessRank;
    createdAt: number;
    profile: {
      location: string;
      country: string;
      lastname: string;
      firstName: string;
      fideRating: number;
    };
    seenAt: number;
    playTime: {
      total: number;
      tv: number;
    };
    url: string;
    playing: string;
    count: {
      all: number;
      rated: number;
      ai: number;
      draw: number;
      drawH: number;
      loss: number;
      lossH: number;
      win: number;
      winH: number;
      bookmark: number;
      playing: number;
      import: number;
      me: number;
    };
    followable: boolean;
    following: boolean;
    blocking: boolean;
    followsYou: boolean;
  };
}

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  whiteList: [],
};
const persistedReducer = persistReducer(persistConfig, reducers);

export const store = configureStore({
  reducer: persistedReducer,
  middleware: [thunk],
});
