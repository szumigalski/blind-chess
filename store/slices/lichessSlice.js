import {createSlice, createAsyncThunk} from '@reduxjs/toolkit';
import axios from 'axios';

export const opponentMove = createAsyncThunk(
  'opponentMove',
  async ({gameId, token, saveMove}) => {
    return await axios
      .get(`https://lichess.org/api/board/game/stream/${gameId}`, {
        headers: {
          Authorization: `Bearer ${token}`,
          responseType: 'stream',
        },
      })
      .then(response => console.log(response.data));
  },
);

export const makeMove = createAsyncThunk(
  'makeMove',
  async ({move, gameId, token}) => {
    await console.log(
      'wuwaj jestem',
      `https://lichess.org/api/board/game/${gameId}/move/${move}`,
    );
    return await axios
      .post(
        `https://lichess.org/api/board/game/${gameId}/move/${move}`,
        {},
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        },
      )
      .then(res => console.log('move!', res))
      .catch(err => {
        console.log('move - err', err);
      });
  },
);

export const resignGame = createAsyncThunk(
  'resignGame',
  async ({gameId, token}) => {
    return await axios
      .post(
        `https://lichess.org/api/board/game/${gameId}/resign`,
        {},
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        },
      )
      .then(res => res)
      .catch(err => {
        console.log('err', err);
      });
  },
);

export const abortGame = createAsyncThunk(
  'abortGame',
  async ({gameId, token}) => {
    return await axios
      .post(
        `https://lichess.org/api/board/game/${gameId}/abort`,
        {},
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        },
      )
      .then(res => res)
      .catch(err => {
        console.log('err', err);
      });
  },
);

export const lichessSlice = createSlice({
  name: 'lichess',
  initialState: {},
  reducers: {},
});

export const {} = lichessSlice.actions;

export default lichessSlice.reducer;
