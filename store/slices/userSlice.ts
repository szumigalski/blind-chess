import {UserReducer} from './../index';
import {createSlice, createAsyncThunk} from '@reduxjs/toolkit';
import axios from 'axios';

export const getAccountInfo = createAsyncThunk(
  'getInfo',
  async ({token}: {token: string}) => {
    return await axios
      .get('https://lichess.org/api/account', {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then(res => res.data)
      .catch(err => {
        console.log('err', err);
        return 'error';
      });
  },
);

export const logout = createAsyncThunk(
  'logout',
  async ({token}: {token: string}) => {
    console.log('dzialam', token);
    return await axios
      .delete('https://lichess.org/api/token', {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then(res => {
        // eslint-disable-next-line no-undef
        toast.show('Wylogowano poprawnie');
        return res.data;
      })
      .catch(err => {
        console.log('err', err);
        // eslint-disable-next-line no-undef
        toast.show('Błąd podczas wylogowywania');
        return 'error';
      });
  },
);

export const userSlice = createSlice({
  name: 'user',
  initialState: {
    token: 'lio_4xmmYytXfmigZkOhBVtu2hBh24Zs6bQ5',
    username: null,
    loading: null,
  } as UserReducer,
  reducers: {
    setToken: (state, action) => {
      console.log('akukuxd', action.payload);
      state.token = action.payload;
    },
  },
  extraReducers: builder => {
    builder.addCase(getAccountInfo.pending, state => {
      state.username = null;
      state.loading = 'loading';
    });
    builder.addCase(getAccountInfo.fulfilled, (state, {payload}) => {
      console.log('hello', payload);
      state.username = payload;
      state.loading = 'loaded';
    });
    builder.addCase(getAccountInfo.rejected, (state, action) => {
      console.log('errrorek');
      state.loading = 'error';
      state.error = action.error.message;
    });
    builder.addCase(logout.pending, state => {
      state.username = null;
      state.username = null;
      state.loading = 'loading';
    });
    builder.addCase(logout.fulfilled, state => {
      state.token = null;
      state.username = null;
      state.loading = 'loaded';
    });
    builder.addCase(logout.rejected, (state, action) => {
      console.log('errrorek');
      state.loading = 'error';
      state.error = action.error.message;
    });
  },
});

export const {setToken} = userSlice.actions;

export default userSlice.reducer;
