import {createSlice} from '@reduxjs/toolkit';
import {ChessSetEnum} from '../../components/icons';
import {ChessColor, GameTypes, OpponentType} from './gamesSlice';

export const settingsSlice = createSlice({
  name: 'settings',
  initialState: {
    status: 'idle',
    error: null,
    gameType: GameTypes.text,
    chessColor: 'white',
    botType: 'lichess',
    chessSet: ChessSetEnum.standard,
    language: undefined,
  } as SettingsInterface,
  reducers: {
    setGameType: (state, action) => {
      state.gameType = action.payload;
    },
    setChessColor: (state, action) => {
      state.chessColor = action.payload;
    },
    setChessSet: (state, action) => {
      state.chessSet = action.payload;
    },
    setLanguage: (state, action) => {
      state.language = action.payload;
    },
    setBotType: (state, action) => {
      state.botType = action.payload;
    },
  },
});

export interface SettingsInterface {
  status: String;
  chessColor: ChessColor;
  gameType: GameTypes;
  error: String | null;
  chessSet: ChessSetEnum;
  language: string | undefined;
  botType: OpponentType;
}

export const {
  setChessColor,
  setGameType,
  setChessSet,
  setLanguage,
  setBotType,
} = settingsSlice.actions;

export enum Languages {
  pl = 'pl',
  en = 'en',
}

export default settingsSlice.reducer;
