import {createSlice, createAsyncThunk} from '@reduxjs/toolkit';
import {isArray} from 'lodash';
import axios from 'axios';

export enum GameTypes {
  text = 'text',
  voice = 'voice',
}

export interface Game {
  botLevel: number;
  pgn: string;
  id: number;
  userMove: string;
  isWrongMove: boolean;
  movesCounter: number;
  lichessId?: string;
  opponentType: OpponentType;
  color: ChessColor;
  lichessBotName?: string;
}

interface NewGame {
  payload: {
    type: GameTypes;
    game: Game;
  };
}

interface UpdatedGame {
  payload: {
    game: Game;
  };
}

export interface GameReducer {
  status: string;
  error: string | null;
  gamesList: Game[];
}

export enum ChessColor {
  white = 'white',
  black = 'black',
}

export enum OpponentType {
  offline = 'offline',
  lichess = 'lichess',
}

export const startNewLichessGame = createAsyncThunk(
  'newGame',
  async ({bot, color, token, level, navigation}) => {
    let body = {
      color: color,
      variant: 'standard',
      fen: 'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1',
      'clock.limit': 3600,
      'clock.increment': 0,
    };
    if (level) {
      body.level = level;
    }
    console.log(body, bot, token);
    return await axios
      .post(`https://lichess.org/api/challenge/${bot}`, body, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then(res => res.data)
      .then(res => {
        console.log('xd', res);
        navigation.navigate('Game');
        toast.show('Utworzono nową grę');
        return {
          ...res,
          aiType: level ? 'stockfish' : 'onlineBot',
          level: level,
        };
      })
      .catch(err => {
        toast.show('Problem podczas tworzenia nowej gry');
        console.log('err', err);
      });
  },
);

export const gameSlice = createSlice({
  name: 'game',
  initialState: {
    status: 'idle',
    gamesList: [
      {
        id: 1,
        botLevel: 2,
        pgn: '',
        movesCounter: 0,
        isWrongMove: false,
        userMove: '',
        opponentType: OpponentType.offline,
        color: ChessColor.white,
      },
    ],
  } as GameReducer,
  reducers: {
    addNewGame: (state, action: NewGame) => {
      state.gamesList.push(action.payload.game);
    },
    updateGame: (state, action: UpdatedGame) => {
      console.log(action.payload.game);
      state.gamesList.splice(
        action.payload.game.id - 1,
        1,
        action.payload.game,
      );
    },
    deleteGame: state => {
      const newVoiceList = state.gamesList.pop();
      if (newVoiceList && isArray(newVoiceList)) {
        state.gamesList = newVoiceList;
      }
    },
  },
  extraReducers: builder => {
    builder.addCase(startNewLichessGame.pending, state => {
      state.status = 'loading';
    });
    builder.addCase(startNewLichessGame.fulfilled, (state, {payload}) => {
      console.log('payload', payload);
      state.gamesList.push({
        id: state.gamesList.length + 1,
        botLevel: 0,
        pgn: '',
        movesCounter: 0,
        isWrongMove: false,
        userMove: '',
        opponentType: OpponentType.lichess,
        color: ChessColor.white,
        lichessId:
          payload.aiType === 'onlineBot' ? payload.challenge.id : payload.id,
        lichessBotName:
          payload.aiType === 'onlineBot'
            ? payload.challenge.destUser.id
            : `stockfish ${payload.level}`,
      });
      console.log('co jest', state.gamesList);
      state.status = 'loaded';
    });
    builder.addCase(startNewLichessGame.rejected, (state, action) => {
      state.status = 'error';
      state.error = action.error.message || null;
    });
  },
});

export const {addNewGame, updateGame, deleteGame} = gameSlice.actions;

export default gameSlice.reducer;
