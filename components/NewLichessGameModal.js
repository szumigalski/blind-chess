import React, {useState, useEffect} from 'react';
import {
  Modal,
  Card,
  RadioGroup,
  Radio,
  Select,
  SelectItem,
  Button,
  Icon,
} from '@ui-kitten/components';
import {StyleSheet, Dimensions, View} from 'react-native';
import {getBotList} from './api';
import {ButtonText, H3} from './StyledComponents';
import {useSelector, useDispatch} from 'react-redux';
import {startNewLichessGame} from '../store/slices/gamesSlice';
import {Slider} from '@miblanchard/react-native-slider';
import {SliderContainer} from './SliderContainer';
import {useTranslation} from 'react-i18next';

const NewLichessGameModal = ({setVisible, visible, navigation}) => {
  const [selectedBot, setSelectedBot] = useState(null);
  const [selectedLevel, setSelectedLevel] = useState(null);
  const [botList, setBotList] = useState([]);
  const [selectedIndex, setSelectedIndex] = useState(0);
  const [rangeDown, setRangeDown] = useState(500);
  const [rangeUp, setRangeUp] = useState(2500);
  const [isDescOpen, setIsDescOpen] = useState(false);

  const stockfishList = [1, 2, 3, 4, 5, 6, 7, 8];
  const {chessColor} = useSelector(state => state.settings);
  const {token} = useSelector(state => state.user);

  const dispatch = useDispatch();
  const {t} = useTranslation();

  useEffect(() => {
    const prepareBots = async () => {
      const bots = await getBotList({token: token});
      await setBotList(bots);
    };
    prepareBots();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const DownIcon = props => (
    <Icon {...props} name="arrow-ios-downward-outline" />
  );

  const UpIcon = props => <Icon {...props} name="arrow-ios-upward-outline" />;

  return (
    <Modal
      visible={visible}
      backdropStyle={styles.modal}
      onBackdropPress={() => setVisible(false)}>
      <Card
        style={{
          width: Dimensions.get('window').width - 20,
        }}>
        <H3>{t('game_settings')}</H3>
        <RadioGroup
          selectedIndex={selectedIndex}
          style={styles.radioButtons}
          onChange={index => {
            setSelectedIndex(index);
            setSelectedBot(null);
          }}>
          <Radio>Stockfish</Radio>
          <Radio>Online bots</Radio>
        </RadioGroup>
        {selectedIndex === 1 ? (
          <SliderContainer
            caption={<ButtonText>{t('choose_rankings_range')}</ButtonText>}
            sliderValue={[rangeDown, rangeUp]}
            setRangeDown={setRangeDown}
            setRangeUp={setRangeUp}>
            <Slider
              minValue={500}
              maximumValue={2500}
              step={100}
              onValueChange={value => console.log({value})}
            />
          </SliderContainer>
        ) : (
          <View />
        )}
        <Select
          value={selectedLevel}
          onSelect={index => {
            console.log(index);
            if (selectedIndex === 0) {
              setSelectedBot(index.row + 1);
              setSelectedLevel(index.row + 1);
            } else {
              const localList = botList.filter(
                el =>
                  el.perfs.correspondence.rating >= rangeDown &&
                  el.perfs.correspondence.rating <= rangeUp,
              );
              console.log(localList[index.row]);
              setSelectedBot(localList[index.row]);
              setSelectedLevel(localList[index.row].username);
            }
          }}>
          {(selectedIndex === 0 ? stockfishList : botList)
            .filter(
              el =>
                !el.perfs ||
                (el.perfs.correspondence.rating >= rangeDown &&
                  el.perfs.correspondence.rating <= rangeUp),
            )
            .map((el, i) => (
              <SelectItem
                title={
                  el.username
                    ? `${el.username} - ${el.perfs.correspondence.rating}`
                    : el
                }
                key={i}
              />
            ))}
        </Select>
        {selectedBot && selectedIndex === 1 ? (
          <>
            <View style={styles.descriptionField}>
              <Button
                accessoryRight={isDescOpen ? UpIcon : DownIcon}
                appearance="ghost"
                onPress={() => setIsDescOpen(state => !state)}
              />
              <ButtonText style={styles.marginRight}>
                {t('show_bot_decription')}
              </ButtonText>
            </View>
            {isDescOpen ? (
              <View style={styles.littlePadding}>
                <ButtonText>{selectedBot.profile.bio}</ButtonText>
              </View>
            ) : (
              <View />
            )}
          </>
        ) : (
          <View />
        )}
        <Button
          appearance="outline"
          disabled={!selectedBot}
          style={styles.marginTop}
          onPress={() => {
            setVisible(false);
            dispatch(
              startNewLichessGame({
                bot: selectedIndex === 1 ? selectedBot.username : 'ai',
                color: chessColor,
                token: token,
                level: selectedIndex === 1 ? null : selectedLevel,
                navigation: navigation,
              }),
            );
          }}>
          {t('start_game')}
        </Button>
      </Card>
    </Modal>
  );
};

const styles = StyleSheet.create({
  modal: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  modalButton: {
    width: 100,
    height: 50,
    marginBottom: 20,
  },
  arrowButton: {
    width: 30,
    height: 30,
    marginTop: 20,
    marginLeft: 50,
    marginRight: 50,
  },
  descriptionField: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: '#eee',
    marginTop: 20,
  },
  marginRight: {
    marginRight: 20,
  },
  marginTop: {
    marginTop: 20,
  },
  littlePadding: {
    padding: 10,
  },
  radioButtons: {
    flexDirection: 'row',
    marginTop: 10,
  },
});

export default NewLichessGameModal;
