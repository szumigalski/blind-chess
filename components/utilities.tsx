import {Svg, Path} from 'react-native-svg';
import React from 'react';
import {OpponentType} from '../store/slices/gamesSlice';
import {makeMove} from '../store/slices/lichessSlice';
import {Languages} from '../store/slices/settingsSlice';

export const lichessHost = 'https://lichess.org';

const addLetterInPosition = (
  string: string,
  letter: string,
  position: number,
) => {
  return [string.slice(0, position), letter, string.slice(position)].join('');
};

export const move = async (
  chess: any,
  userMove: string,
  saveUserMove: Function,
  errorFunction: Function,
  setMovesCounter: Function,
  ai: any,
  botLevel: number,
  setGameOver: Function,
  lang: Languages,
  setIsBotMove: Function,
  track: any,
  opponentType: OpponentType,
  dispatch: Function,
  token: string | null,
  lichessId: string,
) => {
  console.log('halo kurwa');
  let localUserMove = await userMove;
  await errorFunction(false);
  const possibleMoves = await chess.moves();
  if (possibleMoves.includes(userMove + '+')) {
    localUserMove += '+';
  }
  if (possibleMoves.includes(userMove + '#')) {
    localUserMove += '#';
  }
  if (userMove[1] === '1' || userMove[1] === '8') {
    localUserMove = addLetterInPosition(userMove, '=', 2);
  }
  if (userMove[1] === 'x' && (userMove[3] === '8' || userMove[3] === '1')) {
    localUserMove = addLetterInPosition(userMove, '=', 4);
  }
  if (possibleMoves.includes(localUserMove)) {
    await chess.move(localUserMove);
    if (chess.isGameOver()) {
      await setGameOver(true);
    } else {
      const lastMove = await chess.history({verbose: true})[
        chess.history().length - 1
      ];
      console.log('opponentType', opponentType);
      if (opponentType === OpponentType.lichess) {
        await console.log('pwoinien!', token, lichessId);
        await dispatch(
          makeMove({
            move: lastMove.from + lastMove.to,
            gameId: lichessId,
            token: token,
          }),
        );
      } else {
        await ai.move(lastMove.from.toUpperCase(), lastMove.to.toUpperCase());
        await botMove(
          chess,
          ai,
          botLevel,
          setGameOver,
          lang,
          setIsBotMove,
          track,
        );
      }
      await setMovesCounter((state: any) => state + 1);
      await setIsBotMove(true);
      await saveUserMove(localUserMove);
    }
  } else {
    console.log('niepoprawne');
    if (userMove !== '') {
      errorFunction(true);
    }
  }
};

export const botMove = async (
  chess: any,
  ai: any,
  botLevel: number,
  setGameOver: Function,
  lang: Languages,
  setIsBotMove: Function,
  track: any = null,
) => {
  const newMove = await ai.aiMove(botLevel);
  const translatedMove = {
    from: Object.keys(newMove)[0].toLowerCase(),
    to: newMove[Object.keys(newMove)[0]].toLowerCase(),
  };
  await chess.move(translatedMove);
  await setIsBotMove(false);
  if (track) {
    await track.readBotMove(chess.history()[chess.history().length - 1], lang);
  }
  if (chess.isGameOver()) {
    await setGameOver(true);
  }
};

const translateMoveObject = {
  A: 'a',
  B: 'b',
  C: 'c',
  D: 'd',
  E: 'e',
  F: 'f',
  G: 'g',
  H: 'h',
  król: 'K',
  koń: 'N',
  goniec: 'B',
  skoczek: 'N',
  hetman: 'Q',
  królowa: 'Q',
  'krótka roszada': 'O-O',
  'długa roszada': 'O-O-O',
  'Krótka roszada': 'O-O',
  'Długa roszada': 'O-O-O',
  wieża: 'R',
  bije: 'x',
  king: 'K',
  knight: 'N',
  bishop: 'B',
  queen: 'Q',
  'short castle': 'O-O',
  'long castle': 'O-O-O',
  'castle kingside': 'O-O',
  'castle queenside': 'O-O-O',
  rook: 'R',
  takes: 'x',
};

export const translateMove = (translatedmove: string) => {
  let newMove = translatedmove;
  Object.keys(translateMoveObject).forEach(key => {
    if (key in translateMoveObject) {
      newMove = newMove.replaceAll(
        key,
        translateMoveObject[key as keyof typeof translateMoveObject],
      );
    }
  });
  newMove = newMove.replaceAll(' ', '');
  return newMove;
};

export const isNumber = (char: string | number) => {
  if (typeof char !== 'string') {
    return false;
  }

  if (char.trim() === '') {
    return false;
  }

  return char;
};

const convertHistory = (game: any, ai: any) => {
  game.history({verbose: true}).forEach((el: {from: string; to: string}) => {
    ai.move(el.from.toUpperCase(), el.to.toUpperCase());
  });
};

export const loadGame = async (
  pgn: string,
  game: any,
  ai: any,
  setUserMove: Function,
) => {
  if (pgn) {
    let gameHistory;
    await game.loadPgn(pgn);
    gameHistory = await game.history;
    await setUserMove(gameHistory[gameHistory.length - 2]);
    await convertHistory(game, ai);
  }
};

export const checkWhyGameOver = (chess: any) => {
  if (chess.isDraw()) {
    if (chess.isThreefoldRepetition()) {
      return 'draw_three';
    } else if (chess.isInsufficientMaterial()) {
      return 'draw_no_material';
    } else if (chess.isStalemate()) {
      return 'draw_stalemate';
    } else {
      return 'draw_fifty';
    }
  }
  if (chess.moves().length % 2 === 0) {
    return 'black_wins';
  } else {
    return 'white_wins';
  }
};

export const lichessIcon = (size: number) => (
  <Svg height={size} width={size} viewBox="-0.692 0.5 51.573 55.285">
    <Path
      strokeWidth={3}
      fill="#fff"
      d="M38.956.5c-3.53.418-6.452.902-9.286 2.984C5.534 1.786-.692 18.533.68 29.364 3.493 50.214 31.918 55.785 41.329 41.7c-7.444 7.696-19.276 8.752-28.323 3.084S-.506 27.392 4.683 17.567C9.873 7.742 18.996 4.535 29.03 6.405c2.43-1.418 5.225-3.22 7.655-3.187l-1.694 4.86 12.752 21.37c-.439 5.654-5.459 6.112-5.459 6.112-.574-1.47-1.634-2.942-4.842-6.036-3.207-3.094-17.465-10.177-15.788-16.207-2.001 6.967 10.311 14.152 14.04 17.663 3.73 3.51 5.426 6.04 5.795 6.756 0 0 9.392-2.504 7.838-8.927L37.4 7.171z"
    />
  </Svg>
);
