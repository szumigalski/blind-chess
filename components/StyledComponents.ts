import styled from 'styled-components/native';
import {Text} from '@ui-kitten/components';

export const RowView = styled.View`
  flex-direction: row;
  justify-content: space-between;
`;

export const H1 = styled.Text`
  font-family: 'LibreBaskerville-Regular' !important;
  color: ${(props: any) => props.color || 'black'};
  font-size: 25px;
`;

export const H2 = styled.Text`
  font-family: 'LibreBaskerville-Regular' !important;
  font-size: 20px;
  color: ${(props: any) => props.color || 'black'};
`;

export const H3 = styled.Text`
  font-family: 'LibreBaskerville-Regular' !important;
  font-size: 18px;
  color: ${(props: any) => props.color || 'black'};
`;

export const ButtonText = styled.Text`
  font-family: 'LibreBaskerville-Regular' !important;
  font-size: 15px;
  color: ${(props: any) => props.color || 'black'};
`;

export const E = styled.Text`
  font-family: 'LibreBaskerville-Regular' !important;
  font-size: 14px;
  color: ${(props: any) => props.color || 'white'};
`;

export const TopHeader = styled.View`
  flex-direction: row;
  justify-content: space-between;
  background-color: #ddd;
  align-items: center;
  width: 100%;
  position: absolute;
  top: 0px;
  padding: 10px;
  z-index: 999;
`;

export const CenterView = styled.View`
  align-items: center;
`;

export const ChessboardBackground = styled.View`
  flex: 1;
  width: ${(props: any) => props.size || 300}px;
  height: ${(props: any) => props.size || 300}px;
  margin-bottom: 20px;
`;

export const Marker = styled(Text)`
  position: absolute;
  bottom: ${(props: any) =>
    props.type === 'bottom' ? -20 : props.size / 4 + props.size * props.i}px;
  left: ${(props: any) =>
    props.type === 'bottom' ? props.size / 4 + props.size * props.i : -20}px;
  font-size: 14px;
`;

export const PieceImage = styled.Image`
  width: ${(props: any) => props.size / 2 + 10}px;
  height: ${(props: any) => props.size / 2 + 10}px;
  position: absolute;
  top: ${(props: any) => props.top - 5}px;
  left: ${(props: any) => props.left - 5}px;
`;

export const StyledSquare = styled.View`
  flex: 1;
  width: ${(props: any) => props.size / 8}px;
  height: ${(props: any) => props.size / 8}px;
  background-color: ${(props: any) =>
    (props.col + props.offset) % 2 === 1 ? 'white' : 'green'};
`;
