import React, {useEffect, useState} from 'react';
import {Button, Icon} from '@ui-kitten/components';
import {View, Image, StyleSheet} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import {
  setGameType,
  setChessColor,
  setLanguage,
  setBotType,
} from '../store/slices/settingsSlice';
import WhiteKing from './icons/modern/king_white.png';
import BlackKing from './icons/modern/king_black.png';
import SplashScreen from 'react-native-splash-screen';
import {RowView, H1, H2, H3, ButtonText} from './StyledComponents';
import {ReducerInterface} from '../store';
import {useTranslation} from 'react-i18next';
import * as RNLocalize from 'react-native-localize';
import {resources} from './translations/i18n';
import {lichessHost, lichessIcon} from './utilities';
import {setToken} from '../store/slices/userSlice';
import NewLichessGameModal from './NewLichessGameModal';
import {authorize} from 'react-native-app-auth';
import {getAccountInfo} from '../store/slices/userSlice';
import Layout from './Layout';
import NewGameModal from './NewGameModal';

const StartScreen = ({navigation}: any) => {
  const [isLichessModalOpen, setLichessModalOpen] = useState(false);
  const [isNewGameModalOpen, setNewGameModalOpen] = useState(false);

  const {gameType, chessColor, language, botType} = useSelector(
    (state: ReducerInterface) => state.settings,
  );
  const {token, username} = useSelector(
    (state: ReducerInterface) => state.user,
  );
  const dispatch = useDispatch<any>();

  const {t, i18n} = useTranslation();

  useEffect(() => {
    console.log('aktualny stan tokena: ', token, username);
  }, [token, username]);

  const authorizeLichess = async () => {
    const auth = await authorize({
      issuer: `${lichessHost}`,
      clientId: 'blindchess',
      redirectUrl: 'com.blindchessapp:/callback',
      additionalParameters: {},
      scopes: [
        'challenge:write',
        'board:play',
        'bot:play',
        'challenge:read',
        'preference:read',
        'preference:write',
      ],
      serviceConfiguration: {
        authorizationEndpoint: `${lichessHost}/oauth`,
        tokenEndpoint: `${lichessHost}/api/token`,
      },
    });
    if (auth.accessToken) {
      console.log('we have token ', auth.accessToken);
      await dispatch(setToken(auth.accessToken));
      await dispatch(getAccountInfo({token: auth.accessToken}));
    }
  };

  const InfoIcon = (props: any) => (
    <Icon
      {...props}
      width="40px"
      height="40px"
      name="info-outline"
      color="white"
    />
  );

  const SettingsIcon = (props: any) => (
    <Icon
      {...props}
      width="40px"
      height="40px"
      name="settings-2-outline"
      color="white"
    />
  );

  const AiIcon = (props: any) => (
    <Icon
      {...props}
      width="50px"
      height="50px"
      name="hard-drive-outline"
      fill="#fff"
    />
  );

  useEffect(() => {
    RNLocalize.getLocales();
    if (language) {
      i18n.changeLanguage(language);
    } else {
      let localLng = 'en';
      let languagesList = Object.keys(resources);
      RNLocalize.getLocales().every(el => {
        if (languagesList.includes(el.languageCode)) {
          localLng = el.languageCode;
          return false;
        }
      });
      dispatch(setLanguage(localLng));
      i18n.changeLanguage(localLng);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    setTimeout(() => {
      SplashScreen.hide();
    }, 500);
  }, []);

  useEffect(() => {
    const xd = async () => {
      console.log('akuku');
      /*setInterval(() => {
        axios
          .get('https://lichess.org/api/board/game/stream/nnErV0lp', {
            headers: {
              Authorization: 'Bearer lio_4xmmYytXfmigZkOhBVtu2hBh24Zs6bQ5',
              responseType: 'stream',
            },
          })
          .then(response => console.log(response.data));
      }, 5000);*/
    };
    xd();
  }, []);

  return (
    <Layout>
      <View style={styles.header}>
        <H1>Blind Chess</H1>
        <View style={styles.flexRow}>
          <Button
            accessoryLeft={InfoIcon}
            appearance="ghost"
            status="basic"
            style={styles.instructionButton}
            onPress={() => navigation.navigate('Instruction')}
          />
          <Button
            accessoryLeft={SettingsIcon}
            appearance="ghost"
            status="basic"
            style={styles.instructionButton2}
            onPress={() => navigation.navigate('Settings')}
          />
        </View>
      </View>
      <View style={styles.body}>
        <H2 color="white">{t('choose_game_mode')}</H2>
        <RowView>
          <Button
            appearance="outline"
            onPress={() => dispatch(setGameType('voice'))}
            style={styles.gameTypeButton}
            status={gameType === 'voice' ? 'success' : 'basic'}>
            {() => <H3 color="white">{t('voice')}</H3>}
          </Button>
          <Button
            appearance="outline"
            onPress={() => dispatch(setGameType('text'))}
            style={styles.gameTypeButton}
            status={gameType === 'voice' ? 'basic' : 'success'}>
            {() => <H3 color="white">{t('tekst')}</H3>}
          </Button>
        </RowView>
        <H2 color="white">{t('choose_color')}</H2>
        <RowView>
          <Button
            appearance="outline"
            onPress={() => dispatch(setChessColor('white'))}
            style={styles.marginButton}
            status={chessColor === 'white' ? 'success' : 'basic'}>
            <Image source={WhiteKing} />
          </Button>
          <Button
            appearance="outline"
            onPress={() => dispatch(setChessColor('black'))}
            style={styles.marginButton}
            status={chessColor === 'white' ? 'basic' : 'success'}>
            <Image source={BlackKing} />
          </Button>
        </RowView>
        <H2 color="white">{'Wybierz przeciwnika'}</H2>
        <RowView>
          <Button
            appearance="outline"
            onPress={() => dispatch(setBotType('lichess'))}
            style={styles.marginButton}
            status={botType === 'lichess' ? 'success' : 'basic'}>
            {() => (
              <View style={styles.buttonText}>
                {lichessIcon(50)}
                <ButtonText color="white">Lichess bot</ButtonText>
              </View>
            )}
          </Button>
          <Button
            appearance="outline"
            onPress={() => dispatch(setBotType('offline'))}
            style={styles.marginButton}
            status={botType === 'lichess' ? 'basic' : 'success'}>
            {() => (
              <View style={styles.buttonText}>
                <AiIcon />
                <ButtonText color="white">Offline bot</ButtonText>
              </View>
            )}
          </Button>
        </RowView>
      </View>
      <View style={styles.footer}>
        <Button
          appearance="outline"
          style={styles.startGameButton}
          onPress={() => {
            console.log('nasz token', token);
            if (botType === 'offline') {
              navigation.navigate('Game');
            } else {
              if (!token) {
                authorizeLichess();
              } else {
                setNewGameModalOpen(true);
              }
            }
          }}>
          {() => <H2 color="white">{t('start_game')}</H2>}
        </Button>
      </View>
      <NewLichessGameModal
        visible={isLichessModalOpen}
        setVisible={setLichessModalOpen}
        navigation={navigation}
      />
      <NewGameModal
        visible={isNewGameModalOpen}
        setVisible={setNewGameModalOpen}
        navigation={navigation}
        setLichessVisible={setLichessModalOpen}
      />
    </Layout>
  );
};

const styles = StyleSheet.create({
  mainView: {
    flex: 1,
    backgroundColor: '#444',
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
    marginBottom: 10,
    backgroundColor: '#eee',
    padding: 20,
  },
  instructionButton: {
    width: 30,
    height: 30,
  },
  instructionButton2: {
    width: 30,
    height: 30,
    marginLeft: 20,
    marginRight: 10,
  },
  body: {
    padding: 20,
  },
  gameTypeButton: {
    margin: 30,
    width: 120,
    height: 80,
    fontSize: 18,
  },
  startGameButton: {
    width: '90%',
  },
  footer: {
    position: 'absolute',
    bottom: 40,
    left: '20%',
  },
  marginButton: {
    margin: 30,
  },
  buttonText: {
    alignItems: 'center',
  },
  flexRow: {
    flexDirection: 'row',
  },
});

export default StartScreen;
