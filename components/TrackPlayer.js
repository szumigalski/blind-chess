import TrackPlayer from 'react-native-track-player';
import {addTracksToPlayer} from '../voices';

export class TrackPlayerInstance {
  constructor() {
    if (!this.player) {
      this.player = TrackPlayer;
      this.player.setupPlayer();
    }
  }

  async readBotMove(move, lang) {
    let tracklist = await addTracksToPlayer(move, lang);
    await this.player.reset();
    await this.player.add(tracklist);
    await this.player.play();
  }
}
