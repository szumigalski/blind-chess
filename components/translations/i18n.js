import i18n from 'i18next';
import {initReactI18next} from 'react-i18next';
import translationPL from './pl.json';
import translationEN from './en.json';

export const resources = {
  pl: {
    translation: translationPL,
  },
  en: {
    translation: translationEN,
  },
};

i18n.use(initReactI18next).init({
  compatibilityJSON: 'v3',
  resources,
  lng: 'en',
  fallbackLng: 'en',
  interpolation: {
    escapeValue: false,
  },
});
