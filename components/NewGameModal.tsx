import React from 'react';
import {StyleSheet, Dimensions, View} from 'react-native';
import {Modal, Card, Button} from '@ui-kitten/components';
import {ButtonText, H3} from './StyledComponents';
import {ReducerInterface} from '../store';
import {useSelector} from 'react-redux';
import {useTranslation} from 'react-i18next';

const NewGameModal = ({
  visible,
  setVisible,
  navigation,
  setLichessVisible,
}: {
  visible: boolean;
  setVisible: (a: boolean) => void;
  navigation: any;
  setLichessVisible: (a: boolean) => void;
}) => {
  const {gamesList} = useSelector((state: ReducerInterface) => state.game);
  const {t} = useTranslation();

  return (
    <Modal
      visible={visible}
      backdropStyle={styles.modal}
      onBackdropPress={() => setVisible(false)}>
      <Card
        style={{
          width: Dimensions.get('window').width - 20,
        }}>
        <H3>
          {t('continue_game_1') +
            gamesList[0].opponentType +
            t('continue_game_2')}
        </H3>
        <View style={styles.buttonsView}>
          <Button
            style={styles.modalButton}
            onPress={() => setVisible(false)}
            status="danger"
            appearance="outline">
            {() => <ButtonText>{t('cancel')}</ButtonText>}
          </Button>
          <View style={styles.row}>
            <Button
              style={styles.modalButton}
              appearance="outline"
              status="success"
              onPress={() => {
                setVisible(false);
                navigation.navigate('Game');
              }}>
              {() => <ButtonText>{t('load_game')}</ButtonText>}
            </Button>
            <Button
              style={{...styles.modalButton, ...styles.marginLeft}}
              appearance="outline"
              status="success"
              onPress={() => {
                setVisible(false);
                setLichessVisible(true);
              }}>
              {() => <ButtonText>{t('new_game')}</ButtonText>}
            </Button>
          </View>
        </View>
      </Card>
    </Modal>
  );
};

const styles = StyleSheet.create({
  modal: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  modalButton: {
    width: 100,
    height: 50,
    marginBottom: 20,
  },
  arrowButton: {
    width: 30,
    height: 30,
    marginTop: 20,
    marginLeft: 50,
    marginRight: 50,
  },
  buttonsView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 20,
  },
  row: {
    flexDirection: 'row',
  },
  marginLeft: {
    marginLeft: 10,
  },
});

export default NewGameModal;
