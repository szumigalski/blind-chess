import React from 'react';
import {H1, TopHeader} from './StyledComponents';
import {Button, Icon} from '@ui-kitten/components';

const Header = ({name, navigation}: {name: string; navigation: any}) => {
  const BackIcon = (props: any) => (
    <Icon {...props} width="40px" height="40px" name="arrow-back-outline" />
  );

  return (
    <TopHeader>
      <H1>{name}</H1>
      <Button
        accessoryLeft={BackIcon}
        appearance="ghost"
        status="basic"
        onPress={() => navigation.goBack()}
      />
    </TopHeader>
  );
};

export default Header;
