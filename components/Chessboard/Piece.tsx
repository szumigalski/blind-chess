import {sets} from '../icons';

import React from 'react';
import {PieceImage} from '../StyledComponents';
import {ReducerInterface} from '../../store';
import {useSelector} from 'react-redux';

export interface PieceDictionaryInterface {
  q: 'queen_black';
  k: string;
  n: string;
  b: string;
  p: string;
  r: string;
  Q: string;
  K: string;
  N: string;
  B: string;
  P: string;
  R: string;
}

export const pieceDictionary: PieceDictionaryInterface = {
  q: 'queen_black',
  k: 'king_black',
  n: 'knight_black',
  b: 'bishop_black',
  p: 'pawn_black',
  r: 'rook_black',
  Q: 'queen_white',
  K: 'king_white',
  N: 'knight_white',
  B: 'bishop_white',
  P: 'pawn_white',
  R: 'rook_white',
};

interface PieceProps {
  left: number;
  top: number;
  size: number;
  type: keyof PieceDictionaryInterface;
}

export const Piece = ({type, left, top, size}: PieceProps) => {
  const {chessSet} = useSelector((state: ReducerInterface) => state.settings);
  return (
    <PieceImage
      source={sets[chessSet][pieceDictionary[type]]}
      size={size}
      top={top}
      left={left}
    />
  );
};
