import React, {useState, useEffect} from 'react';
import Row from './Row';
import {Pieces} from './Pieces';
import {Modal, Card, Button, Icon} from '@ui-kitten/components';
import {Dimensions, StyleSheet, View} from 'react-native';
import Markers from './Markers';
import {RowView, ChessboardBackground} from '../StyledComponents';
import {Chess} from 'chess.js';

const localChess = new Chess();

interface ChessboardProps {
  pgn: string;
  visible: boolean;
  setVisible(isVisible: boolean): any;
}

const Chessboard = ({pgn, visible, setVisible}: ChessboardProps) => {
  const [isReversed, setIsReversed] = useState(false);
  const [history, setHistory] = useState('');
  const [iterator, setIterator] = useState(0);

  useEffect(() => {
    const prepareLocalChess = async () => {
      await localChess.loadPgn(pgn);
      await setHistory(localChess.history());
      await setIterator(localChess.history().length);
    };
    prepareLocalChess();
  }, [pgn]);

  let chessboardSize = Dimensions.get('window').width - 60;

  const ReverseIcon = (props: any) => <Icon {...props} name="flip-outline" />;

  const CloseIcon = (props: any) => <Icon {...props} name="close-outline" />;

  const LeftIcon = (props: any) => (
    <Icon {...props} name="arrow-ios-back-outline" />
  );

  const RightIcon = (props: any) => (
    <Icon {...props} name="arrow-ios-forward-outline" />
  );

  return (
    <Modal
      visible={visible}
      backdropStyle={styles.modal}
      onBackdropPress={() => setVisible(false)}>
      <Card
        style={{
          width: Dimensions.get('window').width - 20,
        }}>
        <RowView>
          <Button
            accessoryRight={ReverseIcon}
            appearance="outline"
            style={styles.modalButton}
            onPress={() => setIsReversed(state => !state)}>
            Obróć
          </Button>
          <Button
            accessoryRight={CloseIcon}
            appearance="ghost"
            style={styles.modalButton}
            onPress={() => setVisible(false)}
          />
        </RowView>
        <ChessboardBackground size={chessboardSize}>
          {new Array(8).fill(0).map((_, row) => (
            <Row key={row} row={row} size={chessboardSize} />
          ))}
          {localChess.fen().length ? (
            <Pieces
              fen={
                isReversed
                  ? localChess.fen().split('').reverse().join('')
                  : localChess.fen()
              }
              size={chessboardSize}
            />
          ) : (
            <View />
          )}
          <Markers
            size={chessboardSize / 8}
            type={'left'}
            isReversed={isReversed}
          />
          <Markers
            size={chessboardSize / 8}
            type={'bottom'}
            isReversed={isReversed}
          />
        </ChessboardBackground>
        <RowView>
          <Button
            accessoryRight={LeftIcon}
            appearance="outline"
            style={styles.arrowButton}
            disabled={iterator === 0}
            onPress={() => {
              setIterator(state => state - 1);
              localChess.undo();
            }}
          />
          <Button
            accessoryRight={RightIcon}
            appearance="outline"
            style={styles.arrowButton}
            disabled={iterator === history.length}
            onPress={() => {
              console.log(history[iterator], history);
              localChess.move(history[iterator]);
              setIterator(state => state + 1);
            }}
          />
        </RowView>
      </Card>
    </Modal>
  );
};

const styles = StyleSheet.create({
  modal: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  modalButton: {
    width: 100,
    height: 50,
    marginBottom: 20,
  },
  arrowButton: {
    width: 30,
    height: 30,
    marginTop: 20,
    marginLeft: 50,
    marginRight: 50,
  },
});

export default Chessboard;
