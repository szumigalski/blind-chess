import React from 'react';
import {Marker} from '../StyledComponents';

interface MarkersProps {
  type: string;
  size: number;
  isReversed: boolean;
}

const Markers = ({size, type, isReversed}: MarkersProps) => {
  const lettersDictionary = {
    1: 'a',
    2: 'b',
    3: 'c',
    4: 'd',
    5: 'e',
    6: 'f',
    7: 'g',
    8: 'h',
  };

  return (
    <>
      {new Array(8).fill(0).map((_, i) => (
        <Marker i={i} key={Math.random()} type={type} size={size}>
          {isReversed
            ? type === 'bottom'
              ? lettersDictionary[8 - i]
              : 8 - i
            : type === 'bottom'
            ? lettersDictionary[i + 1]
            : i + 1}
        </Marker>
      ))}
    </>
  );
};

export default Markers;
