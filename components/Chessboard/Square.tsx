import React from 'react';
import {StyledSquare} from '../StyledComponents';

interface SquareProps {
  row: number;
  col: number;
  size: number;
}

const Square = ({row, col, size}: SquareProps) => {
  const offset = row % 2 === 0 ? 1 : 0;
  return <StyledSquare offset={offset} col={col} size={size} />;
};

export default Square;
