import {isNumber} from '../utilities';
import {Piece} from './Piece';
import React from 'react';
import {PieceDictionaryInterface} from './Piece';

interface PiecesProps {
  fen: string;
  size: number;
}

export const Pieces = ({fen, size}: PiecesProps) => {
  let squareSize = size / 8;
  let topMargin = squareSize / 4;
  let leftMargin = squareSize / 4;

  return (
    <>
      {fen.split('').map((el: string) => {
        if (isNumber(el)) {
          leftMargin += Number(el) * squareSize;
        } else if (el === '/') {
          topMargin += squareSize;
          leftMargin = squareSize / 4;
        } else {
          leftMargin += squareSize;
          return (
            <Piece
              type={el as keyof PieceDictionaryInterface}
              key={Math.random()}
              top={topMargin}
              left={leftMargin - squareSize}
              size={squareSize}
            />
          );
        }
      })}
    </>
  );
};
