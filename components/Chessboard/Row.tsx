import React from 'react';
import {View, StyleSheet} from 'react-native';
import Square from './Square';

interface RowProps {
  row: number;
  size: number;
}

const Row = ({row, size}: RowProps) => {
  return (
    <View style={styles.row}>
      {new Array(8).fill(0).map((_, col) => (
        <Square key={col} row={row} col={col} size={size} />
      ))}
    </View>
  );
};

const styles = StyleSheet.create({
  row: {
    flex: 1,
    flexDirection: 'row',
    padding: 0,
  },
});

export default Row;
