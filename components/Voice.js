import Voice from '@react-native-voice/voice';
import React, {useEffect, useState} from 'react';
import {translateMove} from './utilities';
import {View, StyleSheet} from 'react-native';
import {Icon, Button} from '@ui-kitten/components';

const VoiceComponent = ({setUserMove, setUserMoved, lang}) => {
  const [isRecording, setIsRecording] = useState(false);

  const onSpeechStart = () => {};

  const onSpeechEnd = () => {};

  const onSpeechResults = e => {
    setUserMove(translateMove(e.value[0]));
    setUserMoved(true);
    stopRecognizing();
  };

  const onSpeechPartialResults = () => {};

  const onSpeechVolumeChanged = () => {};

  const langsDict = {
    pl: 'pl-PL',
    en: 'en-US',
  };

  const startRecognizing = async () => {
    setIsRecording(value => !value);
    try {
      await Voice.start(langsDict[lang]);
    } catch (e) {
      console.log('error:', e);
    }
  };

  const stopRecognizing = async () => {
    setIsRecording(value => !value);
    try {
      await Voice.stop();
    } catch (e) {
      console.log('error: ', e);
    }
  };

  useEffect(() => {
    Voice.onSpeechStart = onSpeechStart;
    Voice.onSpeechEnd = onSpeechEnd;
    Voice.onSpeechResults = onSpeechResults;
    Voice.onSpeechPartialResults = onSpeechPartialResults;
    Voice.onSpeechVolumeChanged = onSpeechVolumeChanged;

    return () => {
      Voice.destroy().then(Voice.removeAllListeners);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const MicIcon = props => (
    <Icon {...props} width="50px" height="50px" name="mic-outline" />
  );
  const StopIcon = props => (
    <Icon {...props} width="50px" height="50px" name="pause-circle" />
  );

  return (
    <View>
      <Button
        accessoryLeft={isRecording ? StopIcon : MicIcon}
        style={styles.buttonCircle}
        disabled={isRecording}
        onPress={() => (!isRecording ? startRecognizing() : stopRecognizing())}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  buttonCircle: {
    width: 200,
    height: 200,
    borderRadius: 100,
  },
});

export default VoiceComponent;
