import axios from 'axios';

export const getBotList = ({token}) => {
  console.log('token: ', token);
  return axios
    .get('https://lichess.org/api/bot/online?nb=90', {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
    .then(res =>
      JSON.parse(
        '[' + res.data.replace(/\r?\n/g, ',').replace(/,\s*$/, '') + ']',
      ),
    )
    .catch(err => {
      console.log('err', err);
    });
};
