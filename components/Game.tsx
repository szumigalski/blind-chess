import {
  Button,
  Select,
  SelectItem,
  Input,
  Spinner,
} from '@ui-kitten/components';
import React, {useEffect, useState} from 'react';
import {View, StyleSheet} from 'react-native';
import {TrackPlayerInstance} from './TrackPlayer';
import {Chess} from 'chess.js';
import {move, botMove, loadGame, checkWhyGameOver} from './utilities';
import VoiceComponent from './Voice';
import {useSelector, useDispatch} from 'react-redux';
import Chessboard from './Chessboard';
import Header from './Header';
import {CenterView, RowView, ButtonText, H3} from './StyledComponents';
import {ReducerInterface} from '../store';
import {Game} from 'js-chess-engine';
import {find} from 'lodash';
import {
  addNewGame,
  ChessColor,
  OpponentType,
  updateGame,
} from '../store/slices/gamesSlice';
import {GameTypes} from '../store/slices/gamesSlice';
import SettingsMenu from './SettingsMenu';
import {useTranslation} from 'react-i18next';
import Layout from './Layout';
import {Languages} from '../store/slices/settingsSlice';

const chess = new Chess();

const track = new TrackPlayerInstance();

let ai = new Game();

const GameComponent = ({navigation}: any) => {
  const [selectedGameId, setSelectedGameId] = useState(1);
  const [userMove, setUserMove] = useState('');
  const [isWrongMove, setIsWrongMove] = useState(false);
  const [movesCounter, setMovesCounter] = useState(0);
  const [isChessboardVisible, setChessboardVisible] = useState(false);
  const [botLevel, setBotLevel] = useState(2);
  const [userMoved, setUserMoved] = useState(false);
  const [isGameOver, setGameOver] = useState(false);
  const [gameOverText, setGameOverText] = useState('');
  const [isBotMove, setIsBotMove] = useState(false);
  const [selectedGame, setSelectedGame] = useState<Game>(null);

  const {chessColor, gameType} = useSelector(
    (state: ReducerInterface) => state.settings,
  );
  const {gamesList} = useSelector((state: ReducerInterface) => state.game);
  const {token} = useSelector((state: ReducerInterface) => state.user);
  const dispatch = useDispatch();
  const {t, i18n} = useTranslation();

  useEffect(() => {
    const localGame = find(gamesList, {id: selectedGameId});
    console.log('localgame:', localGame);
    if (localGame) {
      setIsWrongMove(localGame.isWrongMove);
      ai = new Game();
      loadGame(localGame.pgn, chess, ai, setUserMove);
      setMovesCounter(localGame.movesCounter);
      setBotLevel(localGame.botLevel);
      setSelectedGame(localGame);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedGameId]);

  useEffect(() => {
    if (userMove !== '' && userMoved) {
      move(
        chess,
        userMove,
        setUserMove,
        setIsWrongMove,
        setMovesCounter,
        ai,
        botLevel,
        setGameOver,
        i18n.language as Languages,
        setIsBotMove,
        track,
        selectedGame?.opponentType,
        dispatch,
        token,
        selectedGame?.lichessId,
      );
    }
    setUserMoved(false);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [userMoved]);

  useEffect(() => {
    if (chessColor === 'black' && !chess.history().length) {
      setIsBotMove(true);
      botMove(
        chess,
        ai,
        botLevel,
        setGameOver,
        i18n.language as Languages,
        setIsBotMove,
        gameType === GameTypes.voice ? track : null,
      );
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    console.log('new position: ', chess.pgn());
    dispatch(
      updateGame({
        game: {
          ...selectedGame,
          id: selectedGameId,
          pgn: chess.pgn(),
          isWrongMove: isWrongMove,
          movesCounter: movesCounter,
          botLevel: botLevel,
          userMove: userMove,
          opponentType: selectedGame?.opponentType,
          color: chessColor,
        },
      }),
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [botLevel, isBotMove]);

  useEffect(() => {
    if (isGameOver) {
      setGameOverText(checkWhyGameOver(chess));
    }
  }, [isGameOver]);

  const resetGame = async () => {
    await chess.reset();
    ai = await new Game();
    await setMovesCounter(0);
    await setGameOver(false);
    await dispatch(
      updateGame({
        game: {
          ...selectedGame,
          id: selectedGameId,
          pgn: chess.pgn(),
          isWrongMove: false,
          movesCounter: 0,
          botLevel: botLevel,
          userMove: '',
          opponentType: selectedGame?.opponentType,
          color: ChessColor.white,
        },
      }),
    );
    if (chessColor === ChessColor.black) {
      await setIsBotMove(true);
      await botMove(
        chess,
        ai,
        botLevel,
        setGameOver,
        i18n.language as Languages,
        setIsBotMove,
        gameType === GameTypes.voice ? track : null,
      );
    }
  };

  return (
    <Layout>
      <Header
        name={gameType === GameTypes.voice ? t('set_voice') : t('set_text')}
        navigation={navigation}
      />
      <RowView style={{paddingTop: 100}}>
        <ButtonText color="white" style={styles.marginView}>
          {t('game')} {selectedGameId}
        </ButtonText>
        <Select
          value={t('game') + ' ' + selectedGameId}
          style={styles.selectedGame}
          onSelect={index => {
            setSelectedGameId(Number(index.row + 1));
          }}>
          {gamesList.map((el, i) => (
            <SelectItem title={`${t('game')} ${i + 1}`} key={i} />
          ))}
        </Select>
      </RowView>
      <View style={styles.settings}>
        <SettingsMenu
          navigation={navigation}
          setChessboardVisible={() => setChessboardVisible(true)}
          readLastMove={() =>
            track.readBotMove(
              chess.history()[chess.history().length - 1],
              i18n.language,
            )
          }
          chess={chess}
          createNewGame={() => {
            dispatch(
              addNewGame({
                type: GameTypes.voice,
                game: {
                  id: gamesList.length + 1,
                  botLevel: 2,
                  pgn: '',
                  movesCounter: 0,
                  isWrongMove: false,
                  userMove: '',
                  opponentType: OpponentType.offline,
                  color: chessColor,
                },
              }),
            );
          }}
          resetMove={() => {
            chess.undo();
            chess.undo();
            ai = new Game(ai.getHistory(true)[1].configuration);
            setMovesCounter(state => state - 1);
          }}
          resetGame={resetGame}
          withRead
        />
      </View>
      {gameType === GameTypes.voice ? (
        <CenterView>
          <VoiceComponent
            setUserMove={setUserMove}
            setUserMoved={setUserMoved}
            lang={i18n.language}
          />
        </CenterView>
      ) : (
        <View style={styles.marginView}>
          <RowView>
            <View style={styles.row}>
              <H3 color="white">{t('type_move')}:</H3>
              <Input
                value={userMove}
                style={styles.input}
                onChangeText={nextValue => setUserMove(nextValue)}
                autoCapitalize="none"
              />
            </View>
          </RowView>
          {chess.history().length ? (
            <H3 color="white" style={styles.marginBottom}>
              {t('opponent_last_move')}:{' '}
              {chess.history()[chess.history().length - 1]}
            </H3>
          ) : (
            <View />
          )}
          <Button
            appearance="outline"
            status="success"
            style={styles.marginBottom}
            onPress={() =>
              move(
                chess,
                userMove,
                setUserMove,
                setIsWrongMove,
                setMovesCounter,
                ai,
                botLevel,
                setGameOver,
                i18n.language as Languages,
                setIsBotMove,
                null,
                selectedGame.opponentType,
                dispatch,
                token,
                selectedGame.lichessId,
              )
            }>
            {() => <ButtonText color="white">{t('confirm_move')}</ButtonText>}
          </Button>
        </View>
      )}
      {isBotMove ? (
        <H3 style={styles.botMove} color="white">
          {t('bot_move') || ''} <Spinner />
        </H3>
      ) : (
        <View />
      )}
      {isWrongMove ? (
        <H3 style={styles.redText}>{t('wrong_move') || ''}</H3>
      ) : (
        <View />
      )}
      {isGameOver ? (
        <RowView style={styles.centeredRow}>
          <ButtonText color="white">{t(gameOverText)}</ButtonText>
          <Button
            appearance="outline"
            status="success"
            onPress={() => resetGame()}>
            {() => <ButtonText color="white">{t('new_game')}</ButtonText>}
          </Button>
        </RowView>
      ) : (
        <View />
      )}
      <View>
        <ButtonText color="white" style={styles.marginView}>
          {t('move_counter')}: {movesCounter}
        </ButtonText>
        {selectedGame?.opponentType === 'offline' ? (
          <>
            <ButtonText color="white" style={styles.marginView}>
              {t('bot_level')}:
            </ButtonText>
            <RowView style={{padding: 20}}>
              {[0, 1, 2, 3, 4].map(el => (
                <Button
                  onPress={() => setBotLevel(el)}
                  disabled={botLevel === el}
                  key={el}>
                  {el.toString()}
                </Button>
              ))}
            </RowView>
          </>
        ) : (
          <ButtonText color="white" style={styles.marginView}>
            Lichess bot: {selectedGame?.lichessBotName}
          </ButtonText>
        )}
      </View>
      <Chessboard
        pgn={chess.pgn()}
        visible={isChessboardVisible}
        setVisible={setChessboardVisible}
      />
    </Layout>
  );
};

const styles = StyleSheet.create({
  redText: {
    color: 'red',
    alignSelf: 'center',
  },
  botMove: {
    alignSelf: 'center',
  },
  marginView: {
    padding: 15,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 20,
  },
  input: {
    marginLeft: 30,
  },
  marginBottom: {
    marginBottom: 40,
  },
  centeredRow: {
    padding: 20,
    alignItems: 'center',
  },
  selectedGame: {
    width: 150,
  },
  settings: {
    alignItems: 'flex-end',
  },
});

export default GameComponent;
