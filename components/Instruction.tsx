import React from 'react';
import {ScrollView, View, StyleSheet} from 'react-native';
import Header from './Header';
import Layout from './Layout';
import {H2, H3, E} from './StyledComponents';

const Instruction = ({navigation}: any) => {
  return (
    <View style={styles.layout}>
      <Header name={'Instrukcja'} navigation={navigation} />
      <ScrollView>
        <Layout>
          <E>W szachy na ślepo można grać w dwóch trybach:</E>
          <E> - tekstowym - gdzie można swoje ruchy wpisywać w input</E>
          <E>
            - głosowym - gdzie swoje ruchy można nagrać i odsłuchać ruchów
            przeciwnika
          </E>
          <H2 color="white" style={styles.headerMargin}>
            Rozgrywka
          </H2>
          <E>
            Na ten moment nie ma wgranego silnika szachowego, komputer wykonuje
            losowe ruchy (ale zawsze poprawne!)
          </E>
          <H3 color="white" style={styles.headerMargin}>
            Notacja używana podczas rozgrywki:
          </H3>
          <E>- Król - K</E>
          <E>- Hetman / Królowa - Q</E>
          <E>- Skoczek / Koń - N</E>
          <E>- Wieża - R</E>
          <E>- Bicie - x</E>
          <E>- Krótka roszada - 0-0</E>
          <E>- Długa roszada - 0-0-0</E>
          <E>- Szach - +</E>
          <E>- Mat - #</E>
          <H3 color="white" style={styles.headerMargin}>
            Przykładowe ruchy:
          </H3>
          <E>e4 - Pion na pole e4</E>
          <E>N3e4 - Skoczek z linii 3 na pole e4</E>
          <E>Nde4 - Skoczek z linii d na pole e4</E>
          <E>e8=H - Dojście pionkiem na pole e8 i dorobienie hetmana</E>
        </Layout>
      </ScrollView>
    </View>
  );
};
const styles = StyleSheet.create({
  mainView: {
    padding: 30,
    backgroundColor: '#444',
    marginTop: 80,
  },
  headerMargin: {
    marginTop: 10,
    marginBottom: 20,
  },
  layout: {
    flex: 1,
  },
});
export default Instruction;
