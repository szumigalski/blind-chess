import React from 'react';
import {View, StyleSheet} from 'react-native';
import {useTranslation} from 'react-i18next';
import {Select, SelectItem, Button} from '@ui-kitten/components';
import Header from './Header';
import {ButtonText, H3, PieceImage, RowView} from './StyledComponents';
import {useSelector, useDispatch} from 'react-redux';
import {setChessSet, setLanguage} from '../store/slices/settingsSlice';
import {ChessSetEnum} from './icons';
import {sets} from './icons';
import {logout} from '../store/slices/userSlice';
import {ReducerInterface} from '../store';
import Layout from './Layout';

const Settings = ({navigation}: any) => {
  const {t, i18n}: {i18n: any; t: any} = useTranslation();

  const {chessSet} = useSelector((state: ReducerInterface) => state.settings);
  const {token, username} = useSelector(
    (state: ReducerInterface) => state.user,
  );

  const dispatch = useDispatch<any>();

  const langDictionary = {
    pl: 'polski',
    en: 'english',
  };

  return (
    <Layout>
      <Header name={t('settings')} navigation={navigation} />
      <RowView style={styles.withPadding}>
        <H3 color="white">{t('language')}:</H3>
        <Select
          value={langDictionary[i18n.language]}
          style={styles.selectStyle}
          onSelect={index => {
            dispatch(setLanguage(Object.keys(langDictionary)[index.row]));
            i18n.changeLanguage(Object.keys(langDictionary)[index.row]);
          }}>
          {Object.keys(langDictionary).map((el, i) => (
            <SelectItem title={langDictionary[el]} key={i} />
          ))}
        </Select>
      </RowView>
      <View style={styles.withPadding}>
        <H3 color="white">{t('select_chessboard')}:</H3>
        <Select
          value={chessSet}
          style={styles.piecesStyle}
          onSelect={index => {
            dispatch(
              setChessSet(ChessSetEnum[Object.keys(ChessSetEnum)[index - 1]]),
            );
          }}>
          {Object.keys(ChessSetEnum).map((el, i) => (
            <SelectItem title={ChessSetEnum[el]} key={i} />
          ))}
        </Select>
      </View>
      <View style={styles.piecesBackground}>
        <PieceImage
          size={50}
          top={5}
          left={35}
          source={sets[chessSet].queen_black}
        />
        <PieceImage
          size={50}
          top={5}
          left={75}
          source={sets[chessSet].queen_white}
        />
        <PieceImage
          size={50}
          top={5}
          left={115}
          source={sets[chessSet].king_black}
        />
        <PieceImage
          size={50}
          top={5}
          left={155}
          source={sets[chessSet].king_white}
        />
      </View>
      <View style={styles.withPadding}>
        <H3 color="white">Lichess settings:</H3>
        {token && username ? (
          <View style={styles.margin20}>
            <ButtonText color="white">
              Nazwa użytkownika: {username.username}
            </ButtonText>
            <Button
              appearance="outline"
              status="danger"
              onPress={() => dispatch(logout({token: token}))}
              style={styles.margin20}>
              Wyloguj
            </Button>
          </View>
        ) : (
          <ButtonText style={styles.margin20} color="white">
            Niezalogowany
          </ButtonText>
        )}
      </View>
    </Layout>
  );
};

const styles = StyleSheet.create({
  mainView: {
    flex: 1,
    backgroundColor: '#444',
    paddingTop: 100,
  },
  withPadding: {
    padding: 20,
  },
  selectStyle: {
    width: 150,
  },
  piecesStyle: {
    marginTop: 20,
  },
  piecesBackground: {
    backgroundColor: 'green',
    height: 40,
    marginLeft: 20,
    marginRight: 20,
  },
  margin20: {
    marginTop: 20,
  },
});

export default Settings;
