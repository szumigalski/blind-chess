import King_white_3D from './3D/king_white.png';
import King_black_3D from './3D/king_black.png';
import Queen_white_3D from './3D/queen_white.png';
import Queen_black_3D from './3D/queen_black.png';
import Knight_white_3D from './3D/knight_white.png';
import Knight_black_3D from './3D/knight_black.png';
import Bishop_white_3D from './3D/bishop_white.png';
import Bishop_black_3D from './3D/bishop_black.png';
import Pawn_white_3D from './3D/pawn_white.png';
import Pawn_black_3D from './3D/pawn_black.png';
import Rook_white_3D from './3D/rook_white.png';
import Rook_black_3D from './3D/rook_black.png';

import King_white_Modern from './modern/king_white.png';
import King_black_Modern from './modern/king_black.png';
import Queen_white_Modern from './modern/queen_white.png';
import Queen_black_Modern from './modern/queen_black.png';
import Knight_white_Modern from './modern/knight_white.png';
import Knight_black_Modern from './modern/knight_black.png';
import Bishop_white_Modern from './modern/bishop_white.png';
import Bishop_black_Modern from './modern/bishop_black.png';
import Pawn_white_Modern from './modern/pawn_white.png';
import Pawn_black_Modern from './modern/pawn_black.png';
import Rook_white_Modern from './modern/rook_white.png';
import Rook_black_Modern from './modern/rook_black.png';

import King_white_Neo from './neo/king_white.png';
import King_black_Neo from './neo/king_black.png';
import Queen_white_Neo from './neo/queen_white.png';
import Queen_black_Neo from './neo/queen_black.png';
import Knight_white_Neo from './neo/knight_white.png';
import Knight_black_Neo from './neo/knight_black.png';
import Bishop_white_Neo from './neo/bishop_white.png';
import Bishop_black_Neo from './neo/bishop_black.png';
import Pawn_white_Neo from './neo/pawn_white.png';
import Pawn_black_Neo from './neo/pawn_black.png';
import Rook_white_Neo from './neo/rook_white.png';
import Rook_black_Neo from './neo/rook_black.png';

import King_white_Standard from './standard/king_white.png';
import King_black_Standard from './standard/king_black.png';
import Queen_white_Standard from './standard/queen_white.png';
import Queen_black_Standard from './standard/queen_black.png';
import Knight_white_Standard from './standard/knight_white.png';
import Knight_black_Standard from './standard/knight_black.png';
import Bishop_white_Standard from './standard/bishop_white.png';
import Bishop_black_Standard from './standard/bishop_black.png';
import Pawn_white_Standard from './standard/pawn_white.png';
import Pawn_black_Standard from './standard/pawn_black.png';
import Rook_white_Standard from './standard/rook_white.png';
import Rook_black_Standard from './standard/rook_black.png';

import King_white_Old_fashion from './old_fashion/king_white.png';
import King_black_Old_fashion from './old_fashion/king_black.png';
import Queen_white_Old_fashion from './old_fashion/queen_white.png';
import Queen_black_Old_fashion from './old_fashion/queen_black.png';
import Knight_white_Old_fashion from './old_fashion/knight_white.png';
import Knight_black_Old_fashion from './old_fashion/knight_black.png';
import Bishop_white_Old_fashion from './old_fashion/bishop_white.png';
import Bishop_black_Old_fashion from './old_fashion/bishop_black.png';
import Pawn_white_Old_fashion from './old_fashion/pawn_white.png';
import Pawn_black_Old_fashion from './old_fashion/pawn_black.png';
import Rook_white_Old_fashion from './old_fashion/rook_white.png';
import Rook_black_Old_fashion from './old_fashion/rook_black.png';

const Chess_set_3D = {
  pawn_white: Pawn_white_3D,
  pawn_black: Pawn_black_3D,
  bishop_white: Bishop_white_3D,
  bishop_black: Bishop_black_3D,
  knight_white: Knight_white_3D,
  knight_black: Knight_black_3D,
  queen_black: Queen_black_3D,
  queen_white: Queen_white_3D,
  king_black: King_black_3D,
  king_white: King_white_3D,
  rook_white: Rook_white_3D,
  rook_black: Rook_black_3D,
};

const Chess_set_Modern = {
  pawn_white: Pawn_white_Modern,
  pawn_black: Pawn_black_Modern,
  bishop_white: Bishop_white_Modern,
  bishop_black: Bishop_black_Modern,
  knight_white: Knight_white_Modern,
  knight_black: Knight_black_Modern,
  queen_black: Queen_black_Modern,
  queen_white: Queen_white_Modern,
  king_black: King_black_Modern,
  king_white: King_white_Modern,
  rook_white: Rook_white_Modern,
  rook_black: Rook_black_Modern,
};

const Chess_set_Neo = {
  pawn_white: Pawn_white_Neo,
  pawn_black: Pawn_black_Neo,
  bishop_white: Bishop_white_Neo,
  bishop_black: Bishop_black_Neo,
  knight_white: Knight_white_Neo,
  knight_black: Knight_black_Neo,
  queen_black: Queen_black_Neo,
  queen_white: Queen_white_Neo,
  king_black: King_black_Neo,
  king_white: King_white_Neo,
  rook_white: Rook_white_Neo,
  rook_black: Rook_black_Neo,
};

const Chess_set_Standard = {
  pawn_white: Pawn_white_Standard,
  pawn_black: Pawn_black_Standard,
  bishop_white: Bishop_white_Standard,
  bishop_black: Bishop_black_Standard,
  knight_white: Knight_white_Standard,
  knight_black: Knight_black_Standard,
  queen_black: Queen_black_Standard,
  queen_white: Queen_white_Standard,
  king_black: King_black_Standard,
  king_white: King_white_Standard,
  rook_white: Rook_white_Standard,
  rook_black: Rook_black_Standard,
};

const Chess_set_Old_fashion = {
  pawn_white: Pawn_white_Old_fashion,
  pawn_black: Pawn_black_Old_fashion,
  bishop_white: Bishop_white_Old_fashion,
  bishop_black: Bishop_black_Old_fashion,
  knight_white: Knight_white_Old_fashion,
  knight_black: Knight_black_Old_fashion,
  queen_black: Queen_black_Old_fashion,
  queen_white: Queen_white_Old_fashion,
  king_black: King_black_Old_fashion,
  king_white: King_white_Old_fashion,
  rook_white: Rook_white_Old_fashion,
  rook_black: Rook_black_Old_fashion,
};

export const sets = {
  Chess_set_Old_fashion: Chess_set_Old_fashion,
  Chess_set_3D: Chess_set_3D,
  Chess_set_Modern: Chess_set_Modern,
  Chess_set_Neo: Chess_set_Neo,
  Chess_set_Standard: Chess_set_Standard,
};

export enum ChessSetEnum {
  'three_D' = 'Chess_set_3D',
  'modern' = 'Chess_set_Modern',
  'neo' = 'Chess_set_Neo',
  'standard' = 'Chess_set_Standard',
  'old_fashion' = 'Chess_set_Old_fashion',
}
