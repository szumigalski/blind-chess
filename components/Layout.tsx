import React from 'react';
import {View, StyleSheet} from 'react-native';
import {useNetInfo} from '@react-native-community/netinfo';
import {Icon, Text} from '@ui-kitten/components';

const Layout = ({children}: any) => {
  const netInfo = useNetInfo();
  console.log('net', netInfo.isConnected);
  return (
    <View style={styles.mainView}>
      {!netInfo.isConnected ? (
        <View style={styles.noNetTab}>
          <Text style={styles.noNetText}>No internet connection</Text>
          <Icon
            width="15px"
            height="15px"
            name="wifi-off-outline"
            fill="#000"
          />
        </View>
      ) : (
        <View />
      )}
      {children}
    </View>
  );
};

const styles = StyleSheet.create({
  mainView: {
    flex: 1,
    backgroundColor: '#444',
  },
  noNetTab: {
    position: 'absolute',
    top: 0,
    backgroundColor: '#999',
    height: 20,
    width: 400,
    zIndex: 9999,
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
  },
  noNetText: {
    fontSize: 13,
    marginRight: 10,
  },
});

export default Layout;
