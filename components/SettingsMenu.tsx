import React, {useState} from 'react';
import {Icon, OverflowMenu, MenuItem, Button} from '@ui-kitten/components';
import {View, StyleSheet} from 'react-native';
import {useTranslation} from 'react-i18next';
import {useSelector, useDispatch} from 'react-redux';
import {setGameType} from '../store/slices/settingsSlice';
import {GameTypes} from '../store/slices/gamesSlice';
import {ReducerInterface} from '../store';

interface SettingsMenuProps {
  navigation: any;
  setChessboardVisible: Function;
  readLastMove: Function;
  createNewGame: Function;
  resetMove: Function;
  resetGame: Function;
  chess: any;
  withRead: boolean;
}

const SettingsMenu = ({
  navigation,
  setChessboardVisible,
  readLastMove,
  createNewGame,
  resetMove,
  resetGame,
  chess,
  withRead = false,
}: SettingsMenuProps) => {
  const [visible, setVisible] = useState(false);

  const {t}: {t: (a: string) => string} = useTranslation();

  const {gameType} = useSelector((state: ReducerInterface) => state.settings);

  const dispatch = useDispatch();

  const onItemSelect = (index: {row: number}) => {
    setVisible(false);
    switch (index.row) {
      case 0:
        setChessboardVisible();
        break;
      case 1:
        readLastMove();
        break;
      case 2:
        resetMove();
        break;
      case 3:
        resetGame();
        break;
      case 4:
        createNewGame();
        break;
      case 5:
        navigation.navigate('Settings');
        break;
      case 6:
        dispatch(
          setGameType(
            gameType === GameTypes.voice ? GameTypes.text : GameTypes.voice,
          ),
        );
        break;
      default:
    }
  };

  const CogIcon = (props: any) => (
    <Icon {...props} width="40px" height="40px" name="settings-2-outline" />
  );

  const renderToggleButton = () => (
    <Button
      accessoryLeft={CogIcon}
      appearance="ghost"
      status="basic"
      onPress={() => setVisible(true)}
    />
  );

  return (
    <OverflowMenu
      anchor={renderToggleButton}
      visible={visible}
      style={styles.menuStyle}
      onSelect={onItemSelect}
      onBackdropPress={() => setVisible(false)}>
      <MenuItem title={t('show_chessboard')} />
      {withRead ? (
        <MenuItem title={t('last_move')} disabled={!chess.history().length} />
      ) : (
        <View />
      )}
      <MenuItem title={t('undo_move')} disabled={!chess.history().length} />
      <MenuItem title={t('reset_game')} disabled={!chess.history().length} />
      <MenuItem title={t('add_game')} />
      <MenuItem title={t('settings')} />
      <MenuItem
        title={gameType === GameTypes.voice ? t('set_text') : t('set_voice')}
      />
    </OverflowMenu>
  );
};

const styles = StyleSheet.create({
  menuStyle: {
    maxHeight: 420,
    height: 330,
  },
});

export default SettingsMenu;
