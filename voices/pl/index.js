import a from './a.mp3';
import b from './b.mp3';
import c from './c.mp3';
import d from './d.mp3';
import e from './e.mp3';
import f from './f.mp3';
import g from './g.mp3';
import h from './h.mp3';
import one from './1.mp3';
import two from './2.mp3';
import three from './3.mp3';
import four from './4.mp3';
import five from './5.mp3';
import six from './6.mp3';
import seven from './7.mp3';
import eight from './8.mp3';
import king from './king.mp3';
import knight from './Knight.mp3';
import queen from './Queen.mp3';
import bishop from './Bishop.mp3';
import rook from './Rook.mp3';
import takes from './Takes.mp3';
import shortO from './0-0.mp3';
import longO from './0-0-0.mp3';
import whiteWins from './WhiteWins.mp3';
import blackWins from './BlackWins.mp3';
import draw from './Draw.mp3';

export const soundObject_pl = {
  a: a,
  b: b,
  c: c,
  d: d,
  e: e,
  f: f,
  g: g,
  h: h,
  Q: queen,
  K: king,
  B: bishop,
  R: rook,
  N: knight,
  1: one,
  2: two,
  3: three,
  4: four,
  5: five,
  6: six,
  7: seven,
  8: eight,
  x: takes,
  shortO: shortO,
  longO: longO,
};
