import {Languages} from '../store/slices/settingsSlice';
import {soundObject_en} from './en';
import {soundObject_pl} from './pl';

const soundObject = {
  pl: soundObject_pl,
  en: soundObject_en,
};

export const addTracksToPlayer = async (move, lang = Languages.pl) => {
  if (move === 'O-O') {
    return {id: 0, url: soundObject[lang].shortO};
  } else if (move === 'O-O-O') {
    return {id: 0, url: soundObject[lang].longO};
  } else {
    let letters = await move.split('');
    let trackList = await [];
    await letters.map((element, i) => {
      trackList.push({
        id: i,
        url: soundObject[lang][element],
      });
    });
    return trackList;
  }
};
