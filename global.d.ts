type ToastType = import('react-native-toast-notifications').ToastType;

declare module '*.png' {
  const value: import('react-native').ImageSourcePropType;
  export default value;
}

declare global {
  const toast: ToastType;
}
