import React from 'react';
import * as eva from '@eva-design/eva';
import {ApplicationProvider, IconRegistry} from '@ui-kitten/components';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {EvaIconsPack} from '@ui-kitten/eva-icons';
import StartScreen from './components/StartScreen';
import {store} from './store/index';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';
import {persistStore} from 'redux-persist';
import Game from './components/Game';
import Instruction from './components/Instruction';
import {default as customMapping} from './mapping.json';
import Settings from './components/Settings';
import './components/translations/i18n';
import Toast, {ToastProvider} from 'react-native-toast-notifications';

const persistor = persistStore(store);

const Stack = createNativeStackNavigator();

const App = () => {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <IconRegistry icons={EvaIconsPack} />
        <ApplicationProvider
          {...eva}
          theme={eva.light}
          customMapping={customMapping}>
          <ToastProvider>
            <NavigationContainer>
              <Stack.Navigator>
                <Stack.Screen
                  name="StartScreen"
                  component={StartScreen}
                  options={{
                    headerShown: false,
                  }}
                />
                <Stack.Screen
                  name="Game"
                  component={Game}
                  options={{
                    headerShown: false,
                  }}
                />
                <Stack.Screen
                  name="Instruction"
                  component={Instruction}
                  options={{
                    headerShown: false,
                  }}
                />
                <Stack.Screen
                  name="Settings"
                  component={Settings}
                  options={{
                    headerShown: false,
                  }}
                />
              </Stack.Navigator>
            </NavigationContainer>
          </ToastProvider>
          <Toast ref={ref => (global.toast = ref)} />
        </ApplicationProvider>
      </PersistGate>
    </Provider>
  );
};

export default App;
